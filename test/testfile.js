const sumar = require("../index");
const assert = require("assert");

// Afirmacion

describe("Probar la suma de dos numeros", ()=>{
    // Afirmar que 5+5 = 10
    it('Cinco mas Cinco es Diez', ()=>{
        assert.equal(10, sumar(5,5));
    })
    // Afirmar que 5+5 != 55
    it('Cinco mas Cinco no es Cincuenta y Cinco', ()=>{
        assert.notEqual(55, sumar(5,5));
    })
});